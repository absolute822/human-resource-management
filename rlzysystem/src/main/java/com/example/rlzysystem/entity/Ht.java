package com.example.rlzysystem.entity;

import lombok.Data;

@Data
public class Ht {
    private String id;
    private String yid;
    private String htqx;
    private String begin;
    private String endtime;
    private String content;
    private String name;
}
