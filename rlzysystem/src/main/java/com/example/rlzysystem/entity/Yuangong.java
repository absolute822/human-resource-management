package com.example.rlzysystem.entity;

import lombok.Data;

@Data
public class Yuangong {
    private String id;
    private String name;
    private String render;
    private String age;
    private String phone;
    private String address;
    private String bumen;
    private String zhiwei;
    private String birthday;

}
