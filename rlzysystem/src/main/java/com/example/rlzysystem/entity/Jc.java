package com.example.rlzysystem.entity;

import lombok.Data;

@Data
public class Jc {
    private String id;
    private String yid;
    private String jfrq;
    private String jflx;
    private String jfyy;
    private String remark;
    private String name;
}
