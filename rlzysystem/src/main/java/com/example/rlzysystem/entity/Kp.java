package com.example.rlzysystem.entity;

import lombok.Data;

@Data
public class Kp {
    private String id;
    private String yid;
    private String kprq;
    private String kpjg;
    private String kpnr;
    private String remark;
    private String name;
}
