package com.example.rlzysystem.entity;

import lombok.Data;

@Data
public class Px {
    private String id;
    private String yid;
    private String pxrq;
    private String pxnr;

    private String remark;
    private String name;
}
