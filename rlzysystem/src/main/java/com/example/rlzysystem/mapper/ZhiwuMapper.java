package com.example.rlzysystem.mapper;

import com.example.rlzysystem.entity.Zhiwu;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

import java.util.List;

@Mapper
public interface ZhiwuMapper {
    @Select("select * from zhiwei where bumenid=#{bumenid}")
    List<Zhiwu> select(String bumenid);
}
