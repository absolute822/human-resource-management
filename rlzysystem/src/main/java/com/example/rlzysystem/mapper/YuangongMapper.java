package com.example.rlzysystem.mapper;

import com.example.rlzysystem.entity.Yuangong;
import org.apache.ibatis.annotations.*;

import java.util.List;

@Mapper
public interface YuangongMapper {
    @Select("select * from yuangong where name like concat('%',#{name},'%') limit #{pageNum},#{pageSize}")
    List<Yuangong> select(String name,Integer pageNum,Integer pageSize);
    @Select("select count(*) from yuangong where name like concat('%',#{name},'%')")
    Integer selectcount(String name);
    @Insert("insert into yuangong (name,render,age,phone,address,bumen,zhiwei,birthday) values (#{name},#{render},#{age},#{phone},#{address},#{bumen},#{zhiwei},#{birthday})")
    Integer issave(Yuangong yuangong);
@Update("update yuangong set name=#{name},render=#{render},age=#{age},phone=#{phone},address=#{address},bumen=#{bumen},zhiwei=#{zhiwei},birthday=#{birthday} where id=#{id}")
    Integer isupdate(Yuangong yuangong);
@Delete("delete from yuangong where id=#{id}")
Integer delete(String id);
@Select("select * from yuangong")
    List<Yuangong> selectall();
@Select("select count(*) from yuangong where bumen=#{bumen}")
    Integer selectcount1(String bumen);
}
