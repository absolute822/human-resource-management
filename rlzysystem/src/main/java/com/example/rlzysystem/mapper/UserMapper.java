package com.example.rlzysystem.mapper;

import com.example.rlzysystem.entity.User;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

import java.util.List;

@Mapper
public interface UserMapper {
@Select("select * from user where username = #{username} and password =#{password}")
    List<User> select(String username,String password);

}
