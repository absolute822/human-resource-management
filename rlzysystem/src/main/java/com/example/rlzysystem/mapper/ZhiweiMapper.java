package com.example.rlzysystem.mapper;

import com.example.rlzysystem.entity.Zhiwei;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

import java.util.List;

@Mapper
public interface ZhiweiMapper {
    @Select("select * from bumen")
    List<Zhiwei> select();
}
