package com.example.rlzysystem.mapper;

import com.example.rlzysystem.entity.Jc;
import com.example.rlzysystem.entity.Kp;
import org.apache.ibatis.annotations.*;

import java.util.List;

@Mapper
public interface KpMapper {
    @Select("select * from kp JOIN yuangong on kp.yid = yuangong.id where name like concat('%',#{name},'%') limit #{pageNum},#{pageSize}")
    List<Kp> select(String name, Integer pageNum, Integer pageSize);
    @Insert("insert into kp (yid,kprq,kpjg,kpnr,remark) VALUES (#{yid},#{kprq},#{kpjg},#{kpnr},#{remark})")
    Integer xzadd(Kp kp);
    @Update("update kp set yid=#{yid},kprq=#{kprq},kpjg=#{kpjg},kpnr=#{kpnr},remark=#{remark} where id=#{id}")
    Integer xzupdate(Kp kp);
    @Delete("delete from kp where id=#{id}")
    Integer shanchuxz(String id);
}
