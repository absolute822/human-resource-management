package com.example.rlzysystem.mapper;

import com.example.rlzysystem.entity.Jc;
import com.example.rlzysystem.entity.Xz;
import org.apache.ibatis.annotations.*;

import java.util.List;

@Mapper
public interface JcMapper {
    @Select("select * from jc JOIN yuangong on jc.yid = yuangong.id where name like concat('%',#{name},'%') limit #{pageNum},#{pageSize}")
    List<Jc> select(String name, Integer pageNum, Integer pageSize);
    @Insert("insert into jc (yid,jfrq,jflx,jfyy,remark) VALUES (#{yid},#{jfrq},#{jflx},#{jfyy},#{remark})")
    Integer xzadd(Jc jc);
    @Update("update jc set yid=#{yid},jfrq=#{jfrq},jflx=#{jflx},jfyy=#{jfyy},remark=#{remark} where id=#{id}")
    Integer xzupdate(Jc jc);
    @Delete("delete from jc where id=#{id}")
    Integer shanchuxz(String id);
}
