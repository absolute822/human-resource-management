package com.example.rlzysystem.mapper;

import com.example.rlzysystem.entity.Jc;
import com.example.rlzysystem.entity.Px;
import org.apache.ibatis.annotations.*;

import java.util.List;

@Mapper
public interface PxMapper {
    @Select("select * from px JOIN yuangong on px.yid = yuangong.id where name like concat('%',#{name},'%') limit #{pageNum},#{pageSize}")
    List<Px> select(String name, Integer pageNum, Integer pageSize);
    @Insert("insert into px (yid,pxrq,pxnr,remark) VALUES (#{yid},#{pxrq},#{pxnr},#{remark})")
    Integer xzadd(Px px);
    @Update("update px set yid=#{yid},pxrq=#{pxrq},pxnr=#{pxnr},remark=#{remark} where id=#{id}")
    Integer xzupdate(Px px);
    @Delete("delete from px where id=#{id}")
    Integer shanchuxz(String id);
}
