package com.example.rlzysystem.mapper;

import com.example.rlzysystem.entity.Xz;
import org.apache.ibatis.annotations.*;

import java.util.List;

@Mapper
public interface XzMapper {
    @Select("select * from xinzi JOIN yuangong on xinzi.yid = yuangong.id where name like concat('%',#{name},'%') limit #{pageNum},#{pageSize}")
    List<Xz> select(String name,Integer pageNum,Integer pageSize);
    @Insert("insert into xinzi (yid,xzzt,gz) VALUES (#{yid},#{xzzt},#{gz})")
    Integer xzadd(Xz xz);
    @Update("update xinzi set yid=#{yid},xzzt=#{xzzt},gz=#{gz} where id=#{id}")
    Integer xzupdate(Xz xz);
    @Delete("delete from xinzi where id=#{id}")
    Integer shanchuxz(String id);

}
