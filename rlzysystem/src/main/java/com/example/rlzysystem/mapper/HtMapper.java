package com.example.rlzysystem.mapper;

import com.example.rlzysystem.entity.Ht;
import com.example.rlzysystem.entity.Jc;
import org.apache.ibatis.annotations.*;

import java.util.List;

@Mapper
public interface HtMapper {
    @Select("select * from ht JOIN yuangong on ht.yid = yuangong.id where name like concat('%',#{name},'%') limit #{pageNum},#{pageSize}")
    List<Ht> select(String name, Integer pageNum, Integer pageSize);
    @Insert("insert into ht (yid,htqx,begin,endtime,content) VALUES (#{yid},#{htqx},#{begin},#{endtime},#{content})")
    Integer xzadd(Ht ht);
    @Update("update ht set yid=#{yid},htqx=#{htqx},begin=#{begin},endtime=#{endtime},content=#{content} where id=#{id}")
    Integer xzupdate(Ht ht);
    @Delete("delete from ht where id=#{id}")
    Integer shanchuxz(String id);
}
