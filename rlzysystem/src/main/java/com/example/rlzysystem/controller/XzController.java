package com.example.rlzysystem.controller;

import com.example.rlzysystem.entity.Xz;
import com.example.rlzysystem.entity.Zhiwei;
import com.example.rlzysystem.mapper.XzMapper;
import com.example.rlzysystem.mapper.YuangongMapper;
import com.example.rlzysystem.mapper.ZhiweiMapper;
import lombok.var;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.naming.Name;
import java.util.*;

@RestController
@RequestMapping("xz")
public class XzController {
    @Autowired
    private XzMapper xzMapper;
    @Autowired
    private ZhiweiMapper zhiweiMapper;
    @Autowired
    private YuangongMapper yuangongMapper;
    @GetMapping("/list")
    public Map<String,Object> index(@RequestParam String name,@RequestParam Integer pageNum,@RequestParam Integer pageSize){
Map<String,Object> res=new HashMap<>();
pageNum=(pageNum-1)*pageSize;

        List<Xz> data= xzMapper.select(name,pageNum,pageSize);
        res.put("code",200);
        res.put("data",data);
return res;

    }
    @PostMapping("/save")
    public Map<String,Object> save(@RequestBody Xz xz){
        Map<String,Object> res=new HashMap<>();
        if(xz.getId()==null){
            Integer issave=xzMapper.xzadd(xz);
            if(issave == 1){
                res.put("code",200);
                res.put("msg","新增成功");
            }
        }else{
            Integer issave=xzMapper.xzupdate(xz);
            if(issave == 1){
                res.put("code",200);
                res.put("msg","修改成功");
            }
        }
        return  res;
    }
    @GetMapping("/shanchu")
    public Map<String,Object> shanchu(@RequestParam String id){
        Map<String,Object> res=new HashMap<>();
        Integer issc=xzMapper.shanchuxz(id);
        if(issc == 1){
            res.put("code",200);
            res.put("msg","删除成功");
        }
        return res;
    }
    @GetMapping("/tongji")
    public Map<String,Object> tongji(@RequestParam String bumen){
        Map<String,Object> res=new HashMap<>();
     Integer count= yuangongMapper.selectcount1(bumen);
     res.put("code",200);
     res.put("count",count);
        return res;

    }
}
