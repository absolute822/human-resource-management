package com.example.rlzysystem.controller;

import com.example.rlzysystem.entity.Jc;
import com.example.rlzysystem.entity.Px;
import com.example.rlzysystem.mapper.JcMapper;
import com.example.rlzysystem.mapper.PxMapper;
import com.example.rlzysystem.service.JcService;
import com.example.rlzysystem.service.PxService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("px")
public class PxController {
    @Autowired
    private PxMapper pxMapper;
    @Autowired
    private PxService pxService;
    @GetMapping("/list")
    public Map<String,Object> index(@RequestParam String name, @RequestParam Integer pageNum, @RequestParam Integer pageSize){
        Map<String,Object> res=new HashMap<>();
        pageNum=(pageNum-1)*pageSize;

        List<Px> data= pxMapper.select(name,pageNum,pageSize);
        res.put("code",200);
        res.put("data",data);
        return res;

    }
    @PostMapping("/save")
    public Map<String,Object> save(@RequestBody Px px){
        Map<String,Object> res=new HashMap<>();
        if(px.getId()==null){
            Integer issave= pxService.addxz(px);
            if(issave == 1){
                res.put("code",200);
                res.put("msg","新增成功");
            }
        }else{
            Integer issave=pxService.updatexz(px);
            if(issave == 1){
                res.put("code",200);
                res.put("msg","修改成功");
            }
        }
        return  res;
    }
    @GetMapping("/shanchu")
    public Map<String,Object> shanchu(@RequestParam String id){
        Map<String,Object> res=new HashMap<>();
        Integer issc=pxMapper.shanchuxz(id);
        if(issc == 1){
            res.put("code",200);
            res.put("msg","删除成功");
        }
        return res;
    }
}
