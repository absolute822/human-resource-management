package com.example.rlzysystem.controller;

import com.example.rlzysystem.entity.Zhiwei;
import com.example.rlzysystem.entity.Zhiwu;
import com.example.rlzysystem.mapper.ZhiwuMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("index")
public class ZhiwuController {
    @Autowired
    private ZhiwuMapper zhiwuMapper;
    @GetMapping("/zhiwu")
  public   Map<String,Object> index(@RequestParam String bumenid){

       Map<String,Object> res=new HashMap<>();
        List<Zhiwu> data=zhiwuMapper.select(bumenid);
     res.put("code",200);
       res.put("data",data);
        return res;

    }
}
