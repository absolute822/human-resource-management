package com.example.rlzysystem.controller;

import com.example.rlzysystem.entity.Ht;
import com.example.rlzysystem.entity.Jc;
import com.example.rlzysystem.mapper.HtMapper;
import com.example.rlzysystem.service.HtService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("ht")
public class HtController {
@Autowired
    private HtMapper htMapper;
@Autowired
private HtService htService;
    @GetMapping("/list")
    public Map<String,Object> index(@RequestParam String name, @RequestParam Integer pageNum, @RequestParam Integer pageSize){
        Map<String,Object> res=new HashMap<>();
        pageNum=(pageNum-1)*pageSize;

        List<Ht> data= htMapper.select(name,pageNum,pageSize);
        res.put("code",200);
        res.put("data",data);
        return res;

    }
    @PostMapping("/save")
    public Map<String,Object> save(@RequestBody Ht ht){
        Map<String,Object> res=new HashMap<>();
        if(ht.getId()==null){
            Integer issave= htService.addxz(ht);
            if(issave == 1){
                res.put("code",200);
                res.put("msg","新增成功");
            }
        }else{
            Integer issave=htService.updatexz(ht);
            if(issave == 1){
                res.put("code",200);
                res.put("msg","修改成功");
            }
        }
        return  res;
    }
    @GetMapping("/shanchu")
    public Map<String,Object> shanchu(@RequestParam String id){
        Map<String,Object> res=new HashMap<>();
        Integer issc=htMapper.shanchuxz(id);
        if(issc == 1){
            res.put("code",200);
            res.put("msg","删除成功");
        }
        return res;
    }
}
