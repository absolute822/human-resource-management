package com.example.rlzysystem.controller;

import com.example.rlzysystem.entity.User;
import com.example.rlzysystem.mapper.UserMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/index")
public class UserController {
    @Autowired
    private UserMapper userMapper;
    @GetMapping("/login")
    public Map<String,Object> index(@RequestParam String username,@RequestParam String password){
        Map<String,Object> res=new HashMap<>();
      List<User> data= userMapper.select(username,password);
      if(data.isEmpty()){
          res.put("code",500);

          res.put("msg","该用户不存在");
      }else{
          res.put("code",200);
          res.put("data",data);
          res.put("msg","登录成功");
      }

   return  res;
    }
}
