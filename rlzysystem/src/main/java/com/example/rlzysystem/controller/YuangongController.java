package com.example.rlzysystem.controller;

import com.example.rlzysystem.entity.Yuangong;
import com.example.rlzysystem.mapper.YuangongMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("index")
public class YuangongController {
    @Autowired
    private YuangongMapper yuangongMapper;

    @GetMapping("/yuangong")
    public Map<String,Object> index(@RequestParam String name,@RequestParam Integer pageNum,@RequestParam Integer pageSize){
        Map<String ,Object> res=new HashMap<>();
       pageNum=(pageNum-1)*pageSize;

        List<Yuangong> data=yuangongMapper.select(name,pageNum,pageSize);
        Integer total=yuangongMapper.selectcount(name);
       res.put("code",200);
        res.put("data",data);
        res.put("total",total);
        return res;

    }
    @PostMapping("/save")
    public Map<String,Object> save(@RequestBody Yuangong yuangong){
        Map<String,Object> res=new HashMap<>();
        if(yuangong.getId() == null){
            Integer issave=yuangongMapper.issave(yuangong);
            if(issave == 1){
                res.put("code",200);
                res.put("msg","新增成功");
            }

        }else{
            Integer issave=yuangongMapper.isupdate(yuangong);
            if(issave == 1){
                res.put("code",200);
                res.put("msg","修改成功");
            }
        }
        return  res;

    }
    @GetMapping("/shanchu")
    public Map<String,Object> shanchu(@RequestParam String id){
        Map<String,Object> res=new HashMap<>();
        Integer issc=yuangongMapper.delete(id);
       if(issc == 1){
           res.put("code",200);
           res.put("msg","删除成功");

       }
        return res;

    }
    @GetMapping("/ygall")
    public Map<String ,Object> selectall(){
        Map<String,Object> res=new HashMap<>();
        List<Yuangong> data= yuangongMapper.selectall();
        res.put("code",200);
        res.put("data",data);
        return res;
    }
}
