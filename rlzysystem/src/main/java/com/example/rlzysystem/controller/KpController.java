package com.example.rlzysystem.controller;

import com.example.rlzysystem.entity.Jc;
import com.example.rlzysystem.entity.Kp;
import com.example.rlzysystem.mapper.JcMapper;
import com.example.rlzysystem.mapper.KpMapper;
import com.example.rlzysystem.service.JcService;
import com.example.rlzysystem.service.KpService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("kp")
public class KpController {
    @Autowired
    private KpMapper kpMapper;
    @Autowired
    private KpService kpService;
    @GetMapping("/list")
    public Map<String,Object> index(@RequestParam String name, @RequestParam Integer pageNum, @RequestParam Integer pageSize){
        Map<String,Object> res=new HashMap<>();
        pageNum=(pageNum-1)*pageSize;

        List<Kp> data= kpMapper.select(name,pageNum,pageSize);
        res.put("code",200);
        res.put("data",data);
        return res;

    }
    @PostMapping("/save")
    public Map<String,Object> save(@RequestBody Kp kp){
        Map<String,Object> res=new HashMap<>();
        if(kp.getId()==null){
            Integer issave= kpService.addxz(kp);
            if(issave == 1){
                res.put("code",200);
                res.put("msg","新增成功");
            }
        }else{
            Integer issave=kpService.updatexz(kp);
            if(issave == 1){
                res.put("code",200);
                res.put("msg","修改成功");
            }
        }
        return  res;
    }
    @GetMapping("/shanchu")
    public Map<String,Object> shanchu(@RequestParam String id){
        Map<String,Object> res=new HashMap<>();
        Integer issc=kpMapper.shanchuxz(id);
        if(issc == 1){
            res.put("code",200);
            res.put("msg","删除成功");
        }
        return res;
    }
}
