package com.example.rlzysystem.controller;

import com.example.rlzysystem.entity.Zhiwei;
import com.example.rlzysystem.mapper.ZhiweiMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("index")
public class ZhiweiController {
    @Autowired
    private ZhiweiMapper zhiweiMapper;

    @GetMapping("/zhiwei")
    public Map<String,Object> index(){
        Map<String,Object> res=new HashMap<>();

        List<Zhiwei> data=zhiweiMapper.select();
        res.put("code",200);
        res.put("data",data);


        return  res;

    };
}
