package com.example.rlzysystem;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RlzysystemApplication {

    public static void main(String[] args) {
        SpringApplication.run(RlzysystemApplication.class, args);
    }

}
