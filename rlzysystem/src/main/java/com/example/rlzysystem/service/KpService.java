package com.example.rlzysystem.service;

import com.example.rlzysystem.entity.Jc;
import com.example.rlzysystem.entity.Kp;
import com.example.rlzysystem.mapper.JcMapper;
import com.example.rlzysystem.mapper.KpMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class KpService {
    @Autowired
    private KpMapper kpMapper;
    public  int addxz(Kp kp){
        return  kpMapper.xzadd(kp);
    }
    public  int updatexz(Kp kp){
        return  kpMapper.xzupdate(kp);
    }
}
