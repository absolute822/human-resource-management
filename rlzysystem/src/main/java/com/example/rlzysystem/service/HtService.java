package com.example.rlzysystem.service;

import com.example.rlzysystem.entity.Ht;
import com.example.rlzysystem.entity.Jc;
import com.example.rlzysystem.mapper.HtMapper;
import com.example.rlzysystem.mapper.JcMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class HtService {
    @Autowired
    private HtMapper htMapper;
    public  int addxz(Ht ht){
        return  htMapper.xzadd(ht);
    }
    public  int updatexz(Ht ht){
        return  htMapper.xzupdate(ht);
    }
}
