package com.example.rlzysystem.service;

import com.example.rlzysystem.entity.Yuangong;
import com.example.rlzysystem.mapper.YuangongMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class YuangongService {
    @Autowired
    private YuangongMapper yuangongMapper;
    public int add(Yuangong yuangong){
        return yuangongMapper.issave(yuangong);
    }
    public  int update(Yuangong yuangong){
        return yuangongMapper.isupdate(yuangong);

    }

}
