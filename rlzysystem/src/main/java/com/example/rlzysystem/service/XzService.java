package com.example.rlzysystem.service;

import com.example.rlzysystem.entity.Xz;
import com.example.rlzysystem.mapper.XzMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class XzService {
    @Autowired
    private XzMapper xzMapper;
    public  int addxz(Xz xz){
        return  xzMapper.xzadd(xz);
    }
    public  int updatexz(Xz xz){
        return  xzMapper.xzupdate(xz);
    }
}
