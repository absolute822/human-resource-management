package com.example.rlzysystem.service;

import com.example.rlzysystem.entity.Jc;
import com.example.rlzysystem.entity.Xz;
import com.example.rlzysystem.mapper.JcMapper;
import com.example.rlzysystem.mapper.XzMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class JcService {
    @Autowired
    private JcMapper jcMapper;
    public  int addxz(Jc jc){
        return  jcMapper.xzadd(jc);
    }
    public  int updatexz(Jc jc){
        return  jcMapper.xzupdate(jc);
    }
}
