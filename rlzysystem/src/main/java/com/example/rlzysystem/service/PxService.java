package com.example.rlzysystem.service;

import com.example.rlzysystem.entity.Jc;
import com.example.rlzysystem.entity.Px;
import com.example.rlzysystem.mapper.JcMapper;
import com.example.rlzysystem.mapper.PxMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class PxService {
    @Autowired
    private PxMapper pxMapper;
    public  int addxz(Px px){
        return  pxMapper.xzadd(px);
    }
    public  int updatexz(Px px){
        return  pxMapper.xzupdate(px);
    }
}
