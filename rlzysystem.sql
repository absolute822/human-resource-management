/*
Navicat MySQL Data Transfer

Source Server         : ceshi
Source Server Version : 50505
Source Host           : localhost:3306
Source Database       : rlzysystem

Target Server Type    : MYSQL
Target Server Version : 50505
File Encoding         : 65001

Date: 2023-12-24 22:56:49
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for bumen
-- ----------------------------
DROP TABLE IF EXISTS `bumen`;
CREATE TABLE `bumen` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_nopad_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_nopad_ci;

-- ----------------------------
-- Records of bumen
-- ----------------------------
INSERT INTO `bumen` VALUES ('1', '开发部');
INSERT INTO `bumen` VALUES ('2', '销售部');
INSERT INTO `bumen` VALUES ('3', '总部');

-- ----------------------------
-- Table structure for ht
-- ----------------------------
DROP TABLE IF EXISTS `ht`;
CREATE TABLE `ht` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `yid` varchar(255) COLLATE utf8_unicode_nopad_ci DEFAULT NULL,
  `htqx` varchar(255) COLLATE utf8_unicode_nopad_ci DEFAULT NULL,
  `begin` varchar(255) COLLATE utf8_unicode_nopad_ci DEFAULT NULL,
  `endtime` varchar(255) COLLATE utf8_unicode_nopad_ci DEFAULT NULL,
  `content` varchar(255) COLLATE utf8_unicode_nopad_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_nopad_ci;

-- ----------------------------
-- Records of ht
-- ----------------------------
INSERT INTO `ht` VALUES ('1', '8', '12', '2023-12-14', '2023-12-29', '1233');

-- ----------------------------
-- Table structure for jc
-- ----------------------------
DROP TABLE IF EXISTS `jc`;
CREATE TABLE `jc` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `yid` varchar(255) COLLATE utf8_unicode_nopad_ci DEFAULT '',
  `jfrq` varchar(255) COLLATE utf8_unicode_nopad_ci DEFAULT NULL,
  `jflx` varchar(255) COLLATE utf8_unicode_nopad_ci DEFAULT NULL,
  `jfyy` varchar(255) COLLATE utf8_unicode_nopad_ci DEFAULT NULL,
  `remark` varchar(255) COLLATE utf8_unicode_nopad_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_nopad_ci;

-- ----------------------------
-- Records of jc
-- ----------------------------
INSERT INTO `jc` VALUES ('1', '8', '2023-12-13', '奖励', '奖励，记一功', '123');

-- ----------------------------
-- Table structure for kp
-- ----------------------------
DROP TABLE IF EXISTS `kp`;
CREATE TABLE `kp` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `kprq` varchar(255) COLLATE utf8_unicode_nopad_ci DEFAULT NULL,
  `kpjg` varchar(255) COLLATE utf8_unicode_nopad_ci DEFAULT NULL,
  `kpnr` varchar(255) COLLATE utf8_unicode_nopad_ci DEFAULT NULL,
  `remark` varchar(255) COLLATE utf8_unicode_nopad_ci DEFAULT NULL,
  `yid` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_nopad_ci;

-- ----------------------------
-- Records of kp
-- ----------------------------
INSERT INTO `kp` VALUES ('1', '2023-12-14', '可以转正', '前端面试', '123123', '8');

-- ----------------------------
-- Table structure for px
-- ----------------------------
DROP TABLE IF EXISTS `px`;
CREATE TABLE `px` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `yid` int(11) DEFAULT NULL,
  `pxrq` varchar(255) COLLATE utf8_unicode_nopad_ci DEFAULT NULL,
  `pxnr` varchar(255) COLLATE utf8_unicode_nopad_ci DEFAULT NULL,
  `remark` varchar(255) COLLATE utf8_unicode_nopad_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_nopad_ci;

-- ----------------------------
-- Records of px
-- ----------------------------
INSERT INTO `px` VALUES ('1', '8', '2023-12-21', '前端', '1232');

-- ----------------------------
-- Table structure for user
-- ----------------------------
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) COLLATE utf8_unicode_nopad_ci DEFAULT NULL,
  `password` varchar(255) COLLATE utf8_unicode_nopad_ci DEFAULT NULL,
  `level` varchar(255) COLLATE utf8_unicode_nopad_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_nopad_ci;

-- ----------------------------
-- Records of user
-- ----------------------------
INSERT INTO `user` VALUES ('1', 'admin', '123321', '1');

-- ----------------------------
-- Table structure for xinzi
-- ----------------------------
DROP TABLE IF EXISTS `xinzi`;
CREATE TABLE `xinzi` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `yid` int(11) DEFAULT NULL,
  `xzzt` varchar(255) COLLATE utf8_unicode_nopad_ci DEFAULT NULL,
  `gz` varchar(255) COLLATE utf8_unicode_nopad_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_nopad_ci;

-- ----------------------------
-- Records of xinzi
-- ----------------------------
INSERT INTO `xinzi` VALUES ('2', '8', '营销部工资账套', '5k');

-- ----------------------------
-- Table structure for yuangong
-- ----------------------------
DROP TABLE IF EXISTS `yuangong`;
CREATE TABLE `yuangong` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_nopad_ci DEFAULT NULL,
  `render` varchar(255) COLLATE utf8_unicode_nopad_ci DEFAULT NULL,
  `age` int(11) DEFAULT NULL,
  `phone` varchar(255) COLLATE utf8_unicode_nopad_ci DEFAULT NULL,
  `address` varchar(255) COLLATE utf8_unicode_nopad_ci DEFAULT NULL,
  `bumen` varchar(255) COLLATE utf8_unicode_nopad_ci DEFAULT NULL,
  `zhiwei` varchar(255) COLLATE utf8_unicode_nopad_ci DEFAULT NULL,
  `birthday` varchar(255) COLLATE utf8_unicode_nopad_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_nopad_ci;

-- ----------------------------
-- Records of yuangong
-- ----------------------------
INSERT INTO `yuangong` VALUES ('8', '曾程康', '女', '24', '188034244', '浙江温州', '开发部', 'threeJS', '2002-02-06');

-- ----------------------------
-- Table structure for zhiwei
-- ----------------------------
DROP TABLE IF EXISTS `zhiwei`;
CREATE TABLE `zhiwei` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `zhiweiname` varchar(255) COLLATE utf8_unicode_nopad_ci DEFAULT NULL,
  `bumenid` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_nopad_ci;

-- ----------------------------
-- Records of zhiwei
-- ----------------------------
INSERT INTO `zhiwei` VALUES ('1', '开发工程师', '1');
INSERT INTO `zhiwei` VALUES ('2', '全栈工程师', '1');
INSERT INTO `zhiwei` VALUES ('3', 'threeJS', '1');
INSERT INTO `zhiwei` VALUES ('4', '运营', '2');
INSERT INTO `zhiwei` VALUES ('5', '销售', '4');
