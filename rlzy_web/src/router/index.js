import Vue from 'vue'
import VueRouter from 'vue-router'
import HomeView from '../views/HomeView.vue'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'home',
    component: HomeView
  },
  {
    path: '/about',
    name: 'about',
    redirect: "/index",
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "about" */ '../views/AboutView.vue'),
    children: [{
      path: "/index",
      name: "index",
      component: () => import("../views/index.vue")
    }, {
      path: "/yggl",
      name: "yggl",
      component: () => import("../views/yggl.vue")


    }, {
      path: "/ygglsave",
      name: "ygglsave",
      component: () => import("../views/ygsave.vue")


    }
    , {
      path: "/xzgl",
      name: "xzgl",
      component: () => import("../views/xzgl.vue")


    }, {
      path: "/xzglsave",
      name: "xzglsave",
      component: () => import("../views/xzsave.vue")


    },
    {
      path: "/jcgl",
      name: "jcgl",
      component: () => import("../views/jcgl.vue")


    },
    {
      path: "/jcsave",
      name: "jcsave",
      component: () => import("../views/jcsave.vue")


    },
    {
      path: "/htgl",
      name: "htgl",
      component: () => import("../views/htgl.vue")


    },
    {
      path: "/htsave",
      name: "htsave",
      component: () => import("../views/htsave.vue")


    },
    {
      path: "/pxgl",
      name: "pxgl",
      component: () => import("../views/pxgl.vue")


    },
    {
      path: "/pxsave",
      name: "pxsave",
      component: () => import("../views/pxsave.vue")


    },
    {
      path: "/jxgl",
      name: "jxgl",
      component: () => import("../views/jxgl.vue")


    },
    {
      path: "/kpsave",
      name: "kpsave",
      component: () => import("../views/kpsave.vue")


    },
    
    
    ]
  }
]

const router = new VueRouter({
  routes
})

export default router
